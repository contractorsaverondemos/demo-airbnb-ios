//
//  HostingViewController.swift
//  Airbnb Demo
//
//  Created by Janet Brumbaugh on 5/22/18.
//  Copyright © 2018 Averon. All rights reserved.
//

import UIKit

class HostingViewController: UIViewController {
    
    @IBOutlet weak var dividerView1: UIView!
    
    @IBOutlet weak var dividerView2: UIView!
    
    @IBAction func onClickBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        addBorder( view: dividerView1 )
        addBorder( view: dividerView2 )
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func addBorder( view : UIView ){
        
        let greyBorderColor = UIColor(
            red: 211.0/255.0,
            green: 211.0/255.0,
            blue: 211.0/255.0,
            alpha : 1.0)
        
        view.backgroundColor = .clear
        // view.layer.cornerRadius = 5
        view.layer.borderWidth = 1
        view.layer.borderColor = greyBorderColor.cgColor
    }

}
