//
//  GetConsumerInfoResponse.swift
//  Airbnb Demo
//
//  Created by Janet Brumbaugh on 5/11/18.
//  Copyright © 2018 Averon. All rights reserved.
//

import Foundation


class GetConsumerInfoResponse   : Decodable {
    let status                  : Bool?
    let message                 : String?
    let consumer                : [Consumer]?
}

