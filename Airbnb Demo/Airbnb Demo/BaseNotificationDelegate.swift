//
//  BaseNotificationDelegate.swift
//  Airbnb Demo
//
//  Created by Janet Brumbaugh on 5/18/18.
//  Copyright © 2018 Averon. All rights reserved.
//

import Foundation

protocol BaseNotificationDelegate {
    func onDismiss()
    func onTakePic()
}
