//
//  CongratsViewController.swift
//  Airbnb Demo
//
//  Created by Janet Brumbaugh on 5/18/18.
//  Copyright © 2018 Averon. All rights reserved.
//

import UIKit

class CongratsViewController: BaseNotificationViewController {
     
    @IBAction func onClickReturnToTraveling(_ sender: Any) {
        self.dismiss(animated: true, completion: nil);
        delegate?.onDismiss()
    }
   
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
