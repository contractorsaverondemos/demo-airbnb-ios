//
//  ViewController.swift
//  Airbnb Demo
//
//  Created by Janet Brumbaugh on 5/11/18.
//  Copyright © 2018 Averon. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var btnGetVerified: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var btnViewProfile: UIButton!
    @IBOutlet weak var beReadyToBookView: UIView!
    @IBOutlet weak var verifiedInfoView: UIView!
    
    @IBOutlet weak var btnFacebookConnect: UIButton!
    
  
    @IBOutlet weak var socialView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
  
    @IBOutlet weak var btnGoogleConnect: UIButton!
    
    @IBOutlet weak var btnLinkedInConnect: UIButton!
    
    var isVerifying : Bool = false
    
    let host = "https://api.averon.com"
    let daaPath = "/api/auth/start_daa_authenticate"
    // let daaPath = "/api/auth/start_2FA_authenticate"
    let apiKey = "26FDDF1FCFC2A6F3453B5F3B5FFE00A5"
                 
    let zipCode = "94103"

    let getMdnPath = "/api/auth/get_device_mdn"
    let getConsumerInfoPath = "/api/auth/get_consumer_info"
    let daaSuccessCallbackUrl = "http://www.bobcattechnologies.com"
    let daaAccessCodeCallbackUrl = "http://www.bobcattechnologies.com"
    
    var daaResponse             : DaaResponse?
    var consumerInfoResponse    : GetConsumerInfoResponse?
    
    @IBAction func onClickGetVerified(_ sender: Any) {
        if( isVerifying ){
            return
        }
        startActivityIndicator()
        daaAuthenticate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        customizeIndicator()
    }
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Make sure activity indicator is hidden and animation stopped
        stopActivityIndicator()
     
        customizeScrollView()
        
    }
     
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        var consumer : Consumer?
        
        if( segue.identifier == "TrustAndVerifyToConfirmSegue" ){
            let confirmViewController = segue.destination as! ConfirmViewController
            if( consumerInfoResponse?.consumer != nil ){
                if( (consumerInfoResponse?.consumer?.count)! > 0 ){
                    consumer =  consumerInfoResponse?.consumer![0]
                }
            }
            
            confirmViewController.consumer = consumer
            
        }
      
    }
 
    func daaAuthenticate() {
        
        let urlString = host + daaPath
        var urlComponents = URLComponents(string: urlString)
    
        let escDaaSuccessCallbackUrl = doEncode( input: daaSuccessCallbackUrl )
        let escDaaAccessCodeCallbackUrl =  doEncode( input:daaAccessCodeCallbackUrl)
        
        let parameters =
            [   "X-API-KEY"                 : apiKey,
                // this causes daa=2
                // "phones_list"               : "8312343295",
                "success_callback_url"      : escDaaSuccessCallbackUrl,
                "access_code_callback_url"  : escDaaAccessCodeCallbackUrl ]
        urlComponents = addQueryItems( parameters: parameters, urlComponents: &urlComponents! )
    
    
        // print( "GET request url : ", urlComponents?.url! )
        
        /*
            Create the Request, with the apiKey in the header
        */
        let request = createURLRequest( urlComponents: urlComponents! )
        
        URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
            // Check if data was received successfully
            if error == nil && data != nil {
                do {
                     let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! [String:AnyObject]
                    print( "JSON response = ", json )
                    guard let daaResponse = try? JSONDecoder().decode(DaaResponse.self, from: data!) else {
                        print("Error: Couldn't decode data into DaaResponse")
                    self.stopActivityIndicator()
                    return
                      
                    }
                    self.daaResponse = daaResponse
                    
                    
                    if( self.daaResponse?.status == true ){
                        // Success
                        let decodedEVURL = self.doDecode( input:(self.daaResponse?.url)!)
                        print( "decoded EVURL = ", decodedEVURL )
                        self.evUrl(urlString:decodedEVURL, daaResponse: self.daaResponse! )
                    }
                    else{
                        self.stopActivityIndicator()
                        print( "DAA return status = false" )
                    }
                } catch {
                    self.stopActivityIndicator()
                    print( "An error occurred : data is nil" )
                }
            }
            else if error != nil
            {
                self.stopActivityIndicator()
                print( "Daa Auth failed : ", error?.localizedDescription ?? "Daa Auth failed" )
            }
        }).resume()
    }
    
    
    func evUrl( urlString : String, daaResponse : DaaResponse ){
        let urlComponents = URLComponents(string: urlString )
        let request = createURLRequest( urlComponents: urlComponents! )
        URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
            // Check if data was received successfully
            if error == nil && data != nil {
               
                /*
                self.getConsumerInfo( request_id: daaResponse.request_id )
                */
                if let response = response as? HTTPURLResponse {
                    //Get the refresh header field to determine if the authenticated was successful.
                    let headerFields = response.allHeaderFields
                    let refresh = headerFields["Refresh"] as! String
                    
                    //1 = true, 2 = false.
                    let authenticated = refresh.contains("daa=1")
                    if authenticated {
                        print( "authenticated = " + String( authenticated ) )
                        
                        self.getConsumerInfo( request_id:daaResponse.request_id )
                    }
                }
               
            }
            else if error != nil
            {
               self.stopActivityIndicator()
                print( "Get EVURL failed : error = ", error?.localizedDescription ?? "Get EVURL failed" )
            }
        }).resume()
    }
    
    
    func getMdn( request_id : String ){
        let urlString = host + getMdnPath
        var urlComponents = URLComponents(string: urlString)
        let escRequestId = doEncode( input:request_id )
        
        let parameters =
            [ "request_id"     : escRequestId ]
        urlComponents = addQueryItems( parameters: parameters, urlComponents: &urlComponents! )
        let request = createURLRequestGetConsumerInfo( urlComponents: urlComponents! )
        URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
            // Check if data was received successfully
            
            
            if error == nil && data != nil {
                do {
                    let json = try JSONSerialization.jsonObject(
                        with: data!,
                        options: JSONSerialization.ReadingOptions.mutableContainers) as! [String:AnyObject]
                        print( "JSON response = ", json )
                        self.stopActivityIndicator()
 
                } catch {
                    self.stopActivityIndicator()
                    print( "An error occurred : data is nil" )
                }
            }
            else if error != nil
            {
                self.stopActivityIndicator()
                print( "Get consumer info failed : error = ", error?.localizedDescription ?? "Get consumer info failed" )
            }
        }).resume()
        

    }
    
    func getConsumerInfo( request_id : String ){
        
        /*
        let urlString = host + getConsumerInfoPath
        var urlComponents = URLComponents(string: urlString)
        let escRequestId = doEncode( input:request_id )
        let escZipCode = doEncode( input:zipCode )
        
        let parameters =
            [ "X-API-KEY"    : apiKey,
              "request_id"   : escRequestId,
              "zip_code"     : escZipCode ]
        urlComponents = addQueryItems( parameters: parameters, urlComponents: &urlComponents! )
        let request = createURLRequestGetConsumerInfo( urlComponents: urlComponents! )
        URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
            // Check if data was received successfully
         */
        
        
        /*
        //Basic network configuration.
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: nil, delegateQueue: OperationQueue.main)
        
        let consumerInfoURL = URL(string: "https://api.averon.com/api/auth/get_consumer_info?X-API-KEY=\(apiKey)&request_id=\(request_id)&zip_code=\(zipCode)")!
        
        //Pursue the url returned.
        let task = session.dataTask(with: consumerInfoURL, completionHandler: { [weak self] (data: Data?, response: URLResponse?, error: Error?) -> Void in
        */
        

         let urlString = host + getConsumerInfoPath
         var urlComponents = URLComponents(string: urlString)
         let escRequestId = doEncode( input:request_id )
         let escZipCode = doEncode( input:zipCode )
        
         let parameters =
         [ "request_id"     : escRequestId,
         "zip_code"         : escZipCode ]
         urlComponents = addQueryItems( parameters: parameters, urlComponents: &urlComponents! )
         let request = createURLRequestGetConsumerInfo( urlComponents: urlComponents! )
         URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
         // Check if data was received successfully
         
        
            if error == nil && data != nil {
                do {
                    let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! [String:AnyObject]
                    print( "JSON response = ", json )
                    guard let consumerInfoResponse = try? JSONDecoder().decode(GetConsumerInfoResponse.self, from: data!) else {
                        print("Error: Couldn't decode data into DaaResponse")
                        self.stopActivityIndicator()
                        
                        return
                        
                    }
                    
                    self.consumerInfoResponse = consumerInfoResponse
                    if( consumerInfoResponse.status == true ){
                       
                        DispatchQueue.main.async {
                            self.performSegue(withIdentifier: "TrustAndVerifyToConfirmSegue",
                                              sender: self)
                        }
                        
                    }
                    else{
                        print( "Get consumer info failed : ", consumerInfoResponse.message ?? "Get consumer info failed" )
                        
                        self.stopActivityIndicator()
                        
                        // TEST
                        DispatchQueue.main.async {
                            self.performSegue(withIdentifier: "TrustAndVerifyToConfirmSegue",
                                              sender: self)
                        }
                        
                    }
                } catch {
                    self.stopActivityIndicator()
                    print( "An error occurred : data is nil" )
                }
            }
            else if error != nil
            {
                self.stopActivityIndicator()
                print( "Get consumer info failed : error = ", error?.localizedDescription ?? "Get consumer info failed" )
            }
        }).resume()

    }
    
    func doEncode( input : String ) -> String {
        let encodedString = input.addingPercentEncoding(
        withAllowedCharacters: CharacterSet.urlHostAllowed)
        return encodedString!
    }
    
    
    func doDecode( input : String ) -> String {
        return input.removingPercentEncoding!
    }
    
    func addQueryItems( parameters : Dictionary<String, String>,
                        urlComponents : inout URLComponents ) -> URLComponents? {
        var queryItems = [URLQueryItem]()
        for (key, value) in parameters {
            queryItems.append(URLQueryItem(name: key, value: value))
        }
        urlComponents.queryItems = queryItems
        return urlComponents
    }
    
    func createURLRequest( urlComponents : URLComponents ) -> URLRequest {
        var request = URLRequest(url: (urlComponents.url)!)
        request.httpMethod = "GET"
        
        
        let headers = ["X-API-KEY": apiKey]
        for (key, value) in headers {
            request.setValue(value, forHTTPHeaderField: key)
        }
        
        
        return request
    }
    
    
    func createURLRequestGetConsumerInfo( urlComponents : URLComponents ) -> URLRequest {
        var request = URLRequest(url: (urlComponents.url)!)
        request.httpMethod = "GET"
        
        let headers = ["X-API-KEY": apiKey]
        for (key, value) in headers {
            request.setValue(value, forHTTPHeaderField: key)
        }
        
        
        return request
    }
    
   
    
    func startActivityIndicator() {
        isVerifying = true
        DispatchQueue.main.async {
            self.activityIndicator.isHidden = false
            self.activityIndicator.startAnimating()
        }
    }
    
    func stopActivityIndicator() {
        isVerifying = false
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
            self.activityIndicator.isHidden = true
        }
     
    }
    
    func customizeScrollView() {
        scrollView.layoutIfNeeded()
        scrollView.isScrollEnabled = true
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 1500)
        
        addBorder( view: btnViewProfile )
        addBorder( view: beReadyToBookView )
        addBorder( view: verifiedInfoView )
        addBorder( view: socialView )
        addBorder( view: btnFacebookConnect )
        addBorder( view: btnGoogleConnect )
        addBorder( view: btnLinkedInConnect )
    }
    
    func customizeIndicator() {
        
        // Customize activity indicator, with color and size
        let color = 0xF2646D
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        activityIndicator.color = UIColor(red: red,
                                          green: green,
                                          blue: blue,
                                          alpha : 0xff )
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        let transform: CGAffineTransform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        activityIndicator.transform = transform
        let y = btnGetVerified.bounds.origin.y - 70
        let x = btnGetVerified.center.x
        activityIndicator.center = CGPoint(x:x, y:y )
        
    }
    
    func addBorder( view : UIView ){
        
        let greyBorderColor = UIColor(
            red: 211.0/255.0,
            green: 211.0/255.0,
            blue: 211.0/255.0,
            alpha : 1.0)
        
        view.backgroundColor = .clear
        // view.layer.cornerRadius = 5
        view.layer.borderWidth = 1
        view.layer.borderColor = greyBorderColor.cgColor
    }
    
    
    


}

