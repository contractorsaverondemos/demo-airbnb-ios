//
//  BaseNotificationViewController.swift
//  Airbnb Demo
//
//  Created by Janet Brumbaugh on 5/18/18.
//  Copyright © 2018 Averon. All rights reserved.
//

import UIKit

class BaseNotificationViewController: UIViewController {
    var delegate    : BaseNotificationDelegate?
    
    func setDelegate( _ delegate : BaseNotificationDelegate){
        self.delegate = delegate
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

   

}
