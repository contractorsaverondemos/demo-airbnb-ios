//
//  ConsumerInfo.swift
//  Airbnb Demo
//
//  Created by Janet Brumbaugh on 5/15/18.
//  Copyright © 2018 Averon. All rights reserved.
//

import Foundation

class Consumer : Decodable {
    let ConsumerFirstName           : String = ""
    let ConsumerLastName            : String = ""
    let ConsumerPreferredContact    : String = ""
    let ConsumerAddress1            : String = ""
    let ConsumerAddress2            : String = ""
    let ConsumerCity                : String = ""
    let ConsumerState               : String = ""
    let ConsumerCountryCode         : String = ""
    let ConsumerPostalCode          : String = ""
    let ConsumerMDN                 : String = ""
    let ConsumerDOB                 : String = ""
    let ConsumerEmail               : String = ""
}
