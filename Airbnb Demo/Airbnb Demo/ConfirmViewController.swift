//
//  ConfirmViewController.swift
//  Airbnb Demo
//
//  Created by Janet Brumbaugh on 5/17/18.
//  Copyright © 2018 Averon. All rights reserved.
//

import UIKit

class ConfirmViewController: UIViewController,
    UIPopoverPresentationControllerDelegate,
    BaseNotificationDelegate,
    UINavigationControllerDelegate,
    UIImagePickerControllerDelegate
    {

    // Segues for yes and no are handled automatically
    @IBAction func onClickYesButton(_ sender: Any) {}
    @IBAction func onClickNoButton(_ sender: Any) {}
    
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var lblAddress1: UILabel!
    
    @IBOutlet weak var lblAddress2: UILabel!
    
    @IBOutlet weak var lblCity: UILabel!
    
    @IBOutlet weak var lblState: UILabel!
    
    @IBOutlet weak var lblCountry: UILabel!
    
    @IBOutlet weak var lblPostalCode: UILabel!
    
    @IBOutlet weak var lblPhoneNumber: UILabel!
    
    var consumer : Consumer?

    override func viewDidLoad() {
        super.viewDidLoad()

        if( consumer != nil ){
            lblName.text = (consumer?.ConsumerFirstName)! + " " + (consumer?.ConsumerLastName)!
            
            lblAddress1.text = consumer?.ConsumerAddress1
            lblAddress2.text = consumer?.ConsumerAddress2
            lblCity.text = consumer?.ConsumerCity
            lblState.text = consumer?.ConsumerState
            lblPostalCode.text = consumer?.ConsumerPostalCode
            lblCountry.text = consumer?.ConsumerCountryCode
            
            //lblPhoneNumber.text = consumer?.Pho
            
        }
        else{
            lblName.text = "Alice Smith"
            lblAddress1.text = "100 Cooper Street"
            lblAddress2.text = "Suite 5"
            lblCity.text = "Santa Cruz"
            lblState.text = "CA"
            lblPostalCode.text = "95060"
            lblCountry.text = "USA"
            lblPhoneNumber.text = "(831)425-2458"
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
       // There are only two segues and they are both handled the same way.
        let popoverViewController = segue.destination as! BaseNotificationViewController
        
        popoverViewController.setDelegate( self )
        
        popoverViewController.modalPresentationStyle =  UIModalPresentationStyle.popover
        popoverViewController.popoverPresentationController!.delegate = self
        
        popoverViewController.popoverPresentationController!.sourceView = self.view
    popoverViewController.popoverPresentationController!.permittedArrowDirections = UIPopoverArrowDirection.init(rawValue: 0)
        popoverViewController.popoverPresentationController!.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY,                                                  width: 0,height: 0)
    }
    
    
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    
    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController){
     }
    
    
    func popoverPresentationController(_ popoverPresentationController: UIPopoverPresentationController,
                                       willRepositionPopoverTo rect: UnsafeMutablePointer<CGRect>,
                                       in view: AutoreleasingUnsafeMutablePointer<UIView>){
        // set the source rect just the same as we do in the prepare for segue
        
       
            rect.pointee = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY,width: 0,height: 0)
        
    }
    
    
    func onDismiss () {
        self.dismiss(animated: true, completion: nil)
    }
    
    func onTakePic() {
    
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
        
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info:[String : Any]) {
        
        // this dismisses the image picker
        self.dismiss(animated: true, completion: nil);
        
        self.performSegue(withIdentifier: "ThankYouSegue", sender: self)
        
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        // this dismisses the image picker
        self.dismiss(animated: true, completion: nil);
    }
    
    
  
    
}
