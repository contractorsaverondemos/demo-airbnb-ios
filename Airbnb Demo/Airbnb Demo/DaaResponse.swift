//
//  DaaResponse.swift
//  Airbnb Demo
//
//  Created by Janet Brumbaugh on 5/11/18.
//  Copyright © 2018 Averon. All rights reserved.
//

import Foundation

struct DaaResponse : Decodable {
    let status      : Bool
    let url         : String
    let request_id  : String
    let token       : String
}
