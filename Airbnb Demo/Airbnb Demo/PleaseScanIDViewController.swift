//
//  PleaseScanIDViewController.swift
//  Airbnb Demo
//
//  Created by Janet Brumbaugh on 5/18/18.
//  Copyright © 2018 Averon. All rights reserved.
//

import UIKit

class PleaseScanIDViewController:
    BaseNotificationViewController {
    
    @IBAction func onClickPleaseScanIDButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil);
        delegate?.onTakePic()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

 
 

    
    
}
